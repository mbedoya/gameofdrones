﻿using GOD.Repository;
using GOD.Repository.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOD.Execution.Tests.Common.Mocking
{
    public static class MockHelper
    {
        public static GameDbContext CreateGameDbContext(IQueryable<RoundDbEntity> rounds, IQueryable<GameDbEntity> games)
        {
            Mock<GameDbContext> mockGameDbContext = new Mock<GameDbContext>();

            Mock<DbSet<GameDbEntity>> mockDbEntity = new Mock<DbSet<GameDbEntity>>();
            mockDbEntity.As<IQueryable<GameDbEntity>>().Setup(m => m.Provider).Returns(games.Provider);
            mockDbEntity.As<IQueryable<GameDbEntity>>().Setup(m => m.Expression).Returns(games.Expression);
            mockDbEntity.As<IQueryable<GameDbEntity>>().Setup(m => m.ElementType).Returns(games.ElementType);
            mockDbEntity.As<IQueryable<GameDbEntity>>().Setup(m => m.GetEnumerator()).Returns(games.GetEnumerator());
            mockDbEntity.Setup(x => x.Add(It.IsAny<GameDbEntity>())).Returns((GameDbEntity e) => { e.ID = 1; return e; });
            mockGameDbContext.Setup(x => x.Games).Returns(mockDbEntity.Object);

            Mock<DbSet<StatisticsDbEntity>> mockStatistcsDbEntity = new Mock<DbSet<StatisticsDbEntity>>();            
            mockStatistcsDbEntity.Setup(x => x.Add(It.IsAny<StatisticsDbEntity>())).Returns((StatisticsDbEntity e) => { e.ID = 1; return e; });
            mockGameDbContext.Setup(x => x.Statistics).Returns(mockStatistcsDbEntity.Object);

            Mock<DbSet<RoundDbEntity>> mockRoundDbEntity = new Mock<DbSet<RoundDbEntity>>();
            mockRoundDbEntity.As<IQueryable<RoundDbEntity>>().Setup(m => m.Provider).Returns(rounds.Provider);
            mockRoundDbEntity.As<IQueryable<RoundDbEntity>>().Setup(m => m.Expression).Returns(rounds.Expression);
            mockRoundDbEntity.As<IQueryable<RoundDbEntity>>().Setup(m => m.ElementType).Returns(rounds.ElementType);
            mockRoundDbEntity.As<IQueryable<RoundDbEntity>>().Setup(m => m.GetEnumerator()).Returns(rounds.GetEnumerator());
            mockRoundDbEntity.Setup(x => x.Add(It.IsAny<RoundDbEntity>())).Returns((RoundDbEntity e) => { e.ID = 1; return e; });
            mockGameDbContext.Setup(x => x.GameRounds).Returns(mockRoundDbEntity.Object);

            
            return mockGameDbContext.Object;
        }
    }
}
