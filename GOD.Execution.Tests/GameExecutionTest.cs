﻿using GOD.DTO;
using GOD.Execution.Tests.Common.Mocking;
using GOD.Repository.Entities;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;

namespace GOD.Execution.Tests
{
    [TestFixture]
    public class GameExecutionTest
    {
        GameExecution gameExecution;

        [SetUp]
        public void Setup()
        {
            IQueryable<RoundDbEntity> rounds = new List<RoundDbEntity>() { }.AsQueryable();
            IQueryable<GameDbEntity> games = new List<GameDbEntity>().AsQueryable();
            gameExecution = new GameExecution(MockHelper.CreateGameDbContext(rounds, games));
        }

        [Test]
        public void StartPlay_NamesAreEmpty_ExceptionIsThrown()
        {
            GameContext gameContext = new GameContext();
            gameContext.SessionID = "123";
            gameContext.Player1Name = "";
            gameContext.Player2Name = "";

            Assert.Throws<ArgumentException>( () => gameExecution.StartPlay(gameContext));  
        }

        [Test]
        public void StartPlay_SessionIsEmpty_ExceptionIsThrown()
        {
            GameContext gameContext = new GameContext();
            gameContext.SessionID = "";
            gameContext.Player1Name = "Peter";
            gameContext.Player2Name = "John";            

            Assert.Throws<ArgumentException>(() => gameExecution.StartPlay(gameContext));
        }

        [Test]
        public void StartPlay_SameNames_ExceptionIsThrown()
        {
            GameContext gameContext = new GameContext();
            gameContext.SessionID = "123";
            gameContext.Player1Name = "Peter";
            gameContext.Player2Name = "Peter";

            Assert.Throws<ArgumentException>(() => gameExecution.StartPlay(gameContext));
        }

        [Test]
        public void StartPlay_PlayIsCreated_ReturnIsNewPlay()
        {
            GameContext gameContext = new GameContext();
            gameContext.SessionID = "1234";
            gameContext.Player1Name = "Peter";
            gameContext.Player2Name = "John";

            GameContext resultContext = gameExecution.StartPlay(gameContext);

            Assert.IsNotNull(resultContext);
        }

        [Test]
        public void StartPlay_PlayCreatedContainsID_ReturnIsNewPlay()
        {
            GameContext gameContext = new GameContext();
            gameContext.SessionID = "1234";
            gameContext.Player1Name = "Peter";
            gameContext.Player2Name = "John";

            GameContext resultContext = gameExecution.StartPlay(gameContext);

            Assert.IsTrue(resultContext.GameID > 0);
        }

        [Test]
        public void MakeMove_PaperBeatsRock_ReturnIsMoveResultNotFinished()
        {
            GameMove gameMove = new GameMove();
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Paper;
            gameMove.Player2Move = PossibleMoveActions.Rock;

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(PossibleWinner.P1, result.Winner);
        }

        [Test]
        public void MakeMove_RockBeatsScissors_ReturnIsMoveResultNotFinished()
        {
            GameMove gameMove = new GameMove();
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Rock;
            gameMove.Player2Move = PossibleMoveActions.Scissors;

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(PossibleWinner.P1, result.Winner);
        }

        [Test]
        public void MakeMove_ScissorsBeatsPaper_ReturnIsMoveResultNotFinished()
        {
            GameMove gameMove = new GameMove();
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Scissors;
            gameMove.Player2Move = PossibleMoveActions.Paper;

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(PossibleWinner.P1, result.Winner);
        }

        [Test]
        public void MakeMove_Tie_ReturnIsMoveResultNotFinished()
        {
            GameMove gameMove = new GameMove();
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Paper;
            gameMove.Player2Move = PossibleMoveActions.Paper;

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(PossibleWinner.None, result.Winner);
        }

        [Test]
        public void MakeMove_Player2Wins_ReturnIsMoveResultNotFinished()
        {
            GameMove gameMove = new GameMove();
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Rock;
            gameMove.Player2Move = PossibleMoveActions.Paper;

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(PossibleWinner.P2, result.Winner);
        }

        [Test]
        public void MakeMove_GameNotFinishedMultipleGames_ReturnIsMoveResultNotFinished()
        {
            GameMove gameMove = new GameMove();
            gameMove.GameID = 1;
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Paper;
            gameMove.Player2Move = PossibleMoveActions.Rock;

            IQueryable<RoundDbEntity> rounds = new List<RoundDbEntity>() {
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 2, Winner = "Peter" },
                new RoundDbEntity() { GameId = 2, Winner = "Peter" },
                new RoundDbEntity() { GameId = 3, Winner = "Peter" },
                new RoundDbEntity() { GameId = 3, Winner = "Peter" }
            }.AsQueryable();
            gameExecution = new GameExecution(MockHelper.CreateGameDbContext(rounds, new List<GameDbEntity>().AsQueryable()));

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(false, result.GameFinished);
        }

        [Test]
        public void MakeMove_GameFinishedMultipleGames_ThrowsException()
        {
            GameMove gameMove = new GameMove();
            gameMove.GameID = 1;
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Paper;
            gameMove.Player2Move = PossibleMoveActions.Rock;

            IQueryable<RoundDbEntity> rounds = new List<RoundDbEntity>() {
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 2, Winner = "Peter" },
                new RoundDbEntity() { GameId = 2, Winner = "Peter" },
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 3, Winner = "Peter" }
            }.AsQueryable();
            gameExecution = new GameExecution(MockHelper.CreateGameDbContext(rounds, new List<GameDbEntity>().AsQueryable()));

            Assert.Throws<Exception>(() => gameExecution.MakeMove(gameMove));
        }

        [Test]
        public void MakeMove_NextRoundCountIsCorrect_ReturnIsMoveResult()
        {
            GameMove gameMove = new GameMove();
            gameMove.GameID = 1;
            gameMove.SessionID = "1234";
            gameMove.Player1Move = PossibleMoveActions.Paper;
            gameMove.Player2Move = PossibleMoveActions.Rock;

            IQueryable<RoundDbEntity> rounds = new List<RoundDbEntity>() {
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 1, Winner = "Peter" },
                new RoundDbEntity() { GameId = 2, Winner = "Peter" },
                new RoundDbEntity() { GameId = 2, Winner = "Peter" },
                new RoundDbEntity() { GameId = 1, Winner = "Sara" },
                new RoundDbEntity() { GameId = 3, Winner = "Peter" }
            }.AsQueryable();
            gameExecution = new GameExecution(MockHelper.CreateGameDbContext(rounds, new List<GameDbEntity>().AsQueryable()));

            MoveResult result = gameExecution.MakeMove(gameMove);

            Assert.AreEqual(5, result.NextRound);
        }

        [Test]
        public void GetGameContext_GameContextFound_ReturnIsGameContext()
        {            
            string sessionID = "1234";
            

            IQueryable<GameDbEntity> games = new List<GameDbEntity>() {
                new GameDbEntity() { ID = 1, Player1Name = "Peter", SessionID = "1234" },
                new GameDbEntity() { ID = 1, Player1Name = "Sarah" }                
            }.AsQueryable();
            gameExecution = new GameExecution(MockHelper.CreateGameDbContext(
                new List<RoundDbEntity>() {}.AsQueryable(), 
                games)
                );

            GameContext result = gameExecution.GetGameContext(sessionID);

            Assert.IsNotNull(result);
        }

        [Test]
        public void GetGameContext_PlayerNameIsRight_ReturnIsGameContext()
        {
            string sessionID = "1234";


            IQueryable<GameDbEntity> games = new List<GameDbEntity>() {
                new GameDbEntity() { ID = 1, Player1Name = "Peter", Player2Name = "John", SessionID = "1234" },
                new GameDbEntity() { ID = 1, Player1Name = "Sarah" }
            }.AsQueryable();
            gameExecution = new GameExecution(MockHelper.CreateGameDbContext(
                new List<RoundDbEntity>() { }.AsQueryable(),
                games)
                );

            GameContext result = gameExecution.GetGameContext(sessionID);

            Assert.AreEqual("Peter", result.Player1Name);
            Assert.AreEqual("John", result.Player2Name);
        }
    }
}
