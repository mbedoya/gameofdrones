# README #

### What is this repository for? Game of Drones .Net Website

* It allows 2 People to play Game Of Drones (Paper, Rock, Scissors)
* Multiple sessions supported

### How do I get set up? ###

* Prerrequisites: SQL Server 2014+, Visual Studio 2017+
* Run Script found at /Scripts/DataBaseCreation.sql
* Set Password for user created (god_site_user)
* Modify ConnectionString at GOD.Website project, Web.config file, set Server and Password.

* Want to update code? Run Unit Tests are located at GOD.GameExecution.Tests and be sure nothing is broken.

### Contribution guidelines ###

* No contribution is planned so far.

### Who do I talk to? ###

* Owner: Mauricio Bedoya (mauricio.bedoya@gmail.com)