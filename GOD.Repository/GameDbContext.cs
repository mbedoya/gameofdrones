﻿using GOD.Repository.Entities;
using System.Data.Entity;

namespace GOD.Repository
{
    public class GameDbContext : DbContext
    {
        public virtual DbSet<GameDbEntity> Games { get; set; }
        public virtual DbSet<RoundDbEntity> GameRounds { get; set; }
        public virtual DbSet<StatisticsDbEntity> Statistics { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<GameDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
