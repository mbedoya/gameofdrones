﻿using System.ComponentModel.DataAnnotations;

namespace GOD.Repository.Entities
{
    public class StatisticsDbEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string SessionID { get; set; }

        public int Player1Wins { get; set; }

        public int Player2Wins { get; set; }
    }
}
