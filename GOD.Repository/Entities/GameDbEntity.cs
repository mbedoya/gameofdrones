﻿using System.ComponentModel.DataAnnotations;

namespace GOD.Repository.Entities
{
    public class GameDbEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string SessionID { get; set; }

        public string Player1Name { get; set; }

        public string Player2Name { get; set; }
    }
}
