﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GOD.Repository.Entities
{
    public class RoundDbEntity
    {
        [Key]
        public int ID { get; set; }

        public int RoundNumber { get; set; }

        [Required]
        public string Winner { get; set; }

        [ForeignKey("Game")]
        public int GameId { get; set; }
        public virtual GameDbEntity Game { get; set; }
    }
}
