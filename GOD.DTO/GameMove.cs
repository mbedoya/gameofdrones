﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOD.DTO
{
    public class GameMove
    {
        public int GameID { get; set; }
        public string SessionID { get; set; }
        public PossibleMoveActions Player1Move { get; set; }
        public PossibleMoveActions Player2Move { get; set; }
    }

    public enum PossibleMoveActions
    {
        Paper,
        Rock,
        Scissors
    }
}

