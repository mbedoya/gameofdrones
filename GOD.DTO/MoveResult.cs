﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOD.DTO
{
    public class MoveResult
    {
        public bool GameFinished { get; set; }
        public PossibleWinner Winner { get; set; }
        public int NextRound { get; set; }
    }

    public enum PossibleWinner
    {
        None,
        P1,
        P2
    }
}
