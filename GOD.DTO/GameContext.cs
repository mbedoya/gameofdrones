﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOD.DTO
{
    public class GameContext
    {
        public int GameID { get; set; }
        public string SessionID { get; set; }
        public string Player1Name { get; set; }
        public string Player2Name { get; set; }        
    }
}
