﻿function PlayMovesViewController(dataServices) {

    self = this;

    self.nextViewUrl = "/Play/EndGame";

    self.player1Name = ko.observable("");
    self.player2Name = ko.observable("");

    self.showLoading = ko.observable(false);
    self.loadingText = ko.observable("Loading game info...");

    self.showError = ko.observable(false);
    self.errorText = ko.observable("");

    self.sessionID = ko.observable("");

    self.roundNumber = ko.observable(1);

    self.gameContext = ko.observable();
    self.currentPlayerName = ko.observable("");

    self.getSessionContext = function () {

        dataServices.getGameContext(self.sessionID(), function (success, data) {

            self.showLoading(false);

            if (success) {

                if (data.actionSucceeded) {

                    self.gameContext(data.context);
                    self.currentPlayerName(data.context.Player1Name);

                } else {
                    self.showError(true);
                    self.errorText(data.errorMessage);
                }

            } else {
                self.showError(true);
                self.errorText("Error getting game info");
            }

        });

    }

    self.getSessionID = function () {
        self.sessionID(getParameterByName("session"));        

    }

    self.init = function () {

        self.getSessionID();
        self.getSessionContext();
    }

    self.init();

}