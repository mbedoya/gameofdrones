﻿function PlayHomeDataServices() {

    this.startPlay = function (firstPlayerName, secondPlayerName, sessionID, fx) {
        htttPost("/Play/StartPlay", { Player1Name: firstPlayerName, Player2Name: secondPlayerName, SessionID: sessionID }, function (success, data) {
            fx(success, data);
        });
    }

    this.getSessioniD = function (fx) {
        htttGet("/Play/GetSessionID", function (success, data) {
            fx(success, data);
        });
    }
}