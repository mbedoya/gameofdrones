﻿function PlayHomeViewController(dataServices) {

    self = this;

    self.nextViewUrl = "/Play/Moves";

    self.player1Name = ko.observable("");
    self.player2Name = ko.observable("");

    self.showLoading = ko.observable(false);
    self.loadingText = ko.observable("Starting up the game...");

    self.showError = ko.observable(false);
    self.errorText = ko.observable("");

    self.sessionID = ko.observable("");

    self.startPlay = function () {

        self.showLoading(true);
        self.showError(false);

        dataServices.startPlay(self.player1Name(), self.player2Name(), self.sessionID(), function (success, data) {

            self.showLoading(false);

            if (success) {

                if (data.actionSucceeded) {

                    location.href = self.nextViewUrl + "?session=" + self.sessionID();

                } else {
                    self.showError(true);
                    self.errorText(data.errorMessage);
                }

            } else {
                self.showError(true);
                self.errorText("Error starting play");
            }

        });
    }

    self.getSessionID = function () {
        dataServices.getSessioniD(function (success, data) {

            if (success) {

                console.log(data);
                self.sessionID(data.sessionID);

            } else {
                self.showError(true);
                self.errorText("Error getting session id, unable to play");
            }

        });
    }

    self.init = function () {

        self.getSessionID();
    }

    self.init();

}