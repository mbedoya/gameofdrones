﻿function htttPost(url, data, fx) {
    $.ajax({
        url: url,
        dataType: "json",
        type: "POST",
        data: data,
        success: function (data) {
            fx(true, data);
        },
        error: function (a, b, c) {
            fx(false, a);
        }
    });
}

function htttGet(url, fx) {
    $.ajax({
        url: url,
        dataType: "json",
        type: "GET",
        success: function (data) {
            fx(true, data);
        },
        error: function (a, b, c) {
            fx(false, a);
        }
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

