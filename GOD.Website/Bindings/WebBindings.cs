﻿using GOD.Repository;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GOD.Website.Bindings
{
    public class WebBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<GameDbContext>().To<GameDbContext>();
        }
    }
}