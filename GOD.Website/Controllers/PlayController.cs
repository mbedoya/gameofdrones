﻿using GOD.DTO;
using GOD.Execution;
using GOD.Repository;
using log4net;
using log4net.Repository.Hierarchy;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace GOD.Website.Controllers
{
    public class PlayController : Controller
    {
        GameExecution gameExecution;
        private static readonly ILog logger = LogManager.GetLogger(typeof(PlayController));

        public PlayController()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            var dbContext = kernel.Get<GameDbContext>();
            gameExecution = new GameExecution(dbContext);            
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Moves()
        {
            return View();
        }

        //Session ID will be like a token used for the player for the whole game
        public ActionResult GetSessionID()
        {
            return Json(new { sessionID = Session.SessionID }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGameContext(string sessionID)
        {
            try
            {
                GameContext gameContext = gameExecution.GetGameContext(sessionID);
                return Json(new { actionSucceeded = true, context = gameContext }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Json(new { actionSucceeded = false, errorMessage = ex.Message }, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult StartPlay(GameContext gameContext)
        {
            try
            {
                logger.Debug(String.Format("About to Start a Play ."));
                gameContext.SessionID = gameContext.SessionID;
                gameContext = gameExecution.StartPlay(gameContext);
                logger.Info(String.Format("Play Started."));
            }            
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Json(new { actionSucceeded = false, errorMessage = ex.Message }, JsonRequestBehavior.DenyGet);
            }

            return Json(new { actionSucceeded = true, context = gameContext }, JsonRequestBehavior.DenyGet);
        }        
    }
}