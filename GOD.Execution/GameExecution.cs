﻿using GOD.DTO;
using GOD.Repository;
using GOD.Repository.Entities;
using System;
using System.Linq;

namespace GOD.Execution
{
    public class GameExecution
    {
        private int RoundsCountToWinAGame = 3;
        public GameDbContext dbContext { get; set; }

        public GameExecution(GameDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        private GameDbEntity CreateGame(GameContext gameContext)
        {
            GameDbEntity gameEntity = new GameDbEntity() { Player1Name = gameContext.Player1Name, Player2Name = gameContext.Player2Name, SessionID = gameContext.SessionID };
            gameEntity = dbContext.Games.Add(gameEntity);
            dbContext.SaveChanges();

            return gameEntity;
        }

        public GameContext StartPlay(GameContext gameContext)
        {
            if (String.IsNullOrEmpty(gameContext.Player1Name))
            {
                throw new ArgumentException("Player1 Name must have value");
            }

            if (String.IsNullOrEmpty(gameContext.Player2Name))
            {
                throw new ArgumentException("Player2 Name must have value");
            }

            if (String.Equals(gameContext.Player1Name, gameContext.Player2Name, StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("Player's Names must be different");
            }

            if (String.IsNullOrEmpty(gameContext.SessionID))
            {
                throw new ArgumentException("Session ID must have value");
            }

            GameDbEntity gameEntity = CreateGame(gameContext);
            gameContext.GameID = gameEntity.ID;

            return gameContext;
        }

        private bool GameIsFinished(int gameID)
        {
            var count = (from r
                         in dbContext.GameRounds
                         where r.GameId == gameID
                         group r by r.Winner into grouping
                         where grouping.Count() == RoundsCountToWinAGame
                         select new { Key = grouping.Key, Count = grouping.Count() }).Count();

            return count > 0;
        }

        private int GameRoundsCount(int gameID)
        {
            var count = (from r
                         in dbContext.GameRounds
                         where r.GameId == gameID
                         select r).Count();

            return count;
        }

        private PossibleWinner SelectWinner(GameMove gameMove)
        {
            if (gameMove.Player1Move == gameMove.Player2Move)
            {
                return PossibleWinner.None;
            }

            if (gameMove.Player1Move == PossibleMoveActions.Paper && gameMove.Player2Move == PossibleMoveActions.Rock ||
                gameMove.Player1Move == PossibleMoveActions.Rock && gameMove.Player2Move == PossibleMoveActions.Scissors ||
                gameMove.Player1Move == PossibleMoveActions.Scissors && gameMove.Player2Move == PossibleMoveActions.Paper)
            {
                return PossibleWinner.P1;
            }

            return PossibleWinner.P2;
        }

        public int SaveRound(GameMove gameMove, PossibleWinner winner)
        {
            RoundDbEntity round = new RoundDbEntity();
            round.GameId = gameMove.GameID;
            round.Winner = winner.ToString();
            round.RoundNumber = GameRoundsCount(gameMove.GameID) + 1;
            dbContext.GameRounds.Add(round);
            dbContext.SaveChanges();

            return round.RoundNumber;
        }

        private StatisticsDbEntity GetSessionStatistics(string sessionID)
        {
            return (from s in dbContext.Statistics
                    where s.SessionID == sessionID
                    select s).FirstOrDefault();
        }


        private void UpdateStatistics(GameMove gameMove, PossibleWinner winner)
        {
            StatisticsDbEntity statsEntity = GetSessionStatistics(gameMove.SessionID);
            bool statsFound = statsEntity != null;

            if (statsEntity == null)
            {
                statsEntity = new StatisticsDbEntity();
                statsEntity.SessionID = gameMove.SessionID;
            }

            if (winner == PossibleWinner.P1)
            {
                statsEntity.Player1Wins ++;
            }
            else
            {
                statsEntity.Player2Wins ++;
            }

            if (!statsFound)
            {
                dbContext.Statistics.Add(statsEntity);
            }
            else
            {
                dbContext.Entry(statsEntity).CurrentValues.SetValues(statsEntity);
            }

            dbContext.SaveChanges();
        }

        public MoveResult MakeMove(GameMove gameMove)
        {
            if (GameIsFinished(gameMove.GameID))
            {
                throw new Exception(String.Format("Game {0}, had already finished", gameMove.GameID));
            }

            PossibleWinner winner = SelectWinner(gameMove);

            int savedRoundCount = SaveRound(gameMove, winner);            

            bool gameFinishedAfterRound = GameIsFinished(gameMove.GameID);
            if (gameFinishedAfterRound)
            {
                UpdateStatistics(gameMove, winner);
            }

            MoveResult result = new MoveResult();
            result.GameFinished = gameFinishedAfterRound;
            result.Winner = winner;
            result.NextRound = savedRoundCount + 1;

            return result;
        }

        public GameContext GetGameContext(string sessionID)
        {
            var result = (from p
                         in dbContext.Games
                         where p.SessionID == sessionID
                         select new GameContext(){
                             GameID = p.ID,
                             SessionID = sessionID,
                             Player1Name = p.Player1Name,
                             Player2Name = p.Player2Name }).FirstOrDefault();

            return result;
        }
    }
}
